const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './tests/*_test.js',
  output: './reports/output',
  helpers: {
    Puppeteer: {
      url: 'http://contorion.de',
      show: true,
      windowSize: '1280x960',
      waitForNavigation: "networkidle0"
    }
  },
  Mochawesome: {
    uniqueScreenshotNames: "true",
    reportFileName: 'reportUI'
  },
  mocha: {
    reporterOptions: {
        reportDir: "./reports/output"
    }
  },
  include: {
    I: './steps_file.js',
    homePage: './pages/homePage.js',
    loginPage: './pages/loginPage.js',
  },
  bootstrap: null,
  name: 'contorion',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    customLocator: {
      enabled: true,
      attribute: 'data-qa'
    }
  }
}