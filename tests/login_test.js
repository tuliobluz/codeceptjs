const homePage = require("../pages/homePage");
const testData = require('../utils/testData.js');

Feature('Login');

Scenario('Successfully Login', ({ I, homePage, loginPage}) => {
    loginPage.goToLoginPage();
    homePage.acceptCookies();
    loginPage.fillLogin(testData.variables.USER);
    loginPage.submitLogin();

    homePage.successLoginMsg();
});
