const { I } = inject();

module.exports = {
  cookiesButton: '#popin_tc_privacy_button',
  successLogin: '#content .js-notification-message',

  acceptCookies(){
    I.click(this.cookiesButton);
  },

  successLoginMsg(){
    I.seeElement(this.successLogin)
  },
}