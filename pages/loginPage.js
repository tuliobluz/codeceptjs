const { I } = inject();

module.exports = {
  fields: {
    email: '$login[email]',
    password: '$login[password]',
  },
  loginButton: '$loginButtonSubmit',
  
  goToLoginPage(){
    I.amOnPage('/login');
  },

  fillLogin(user) {
    I.fillField(this.fields.email, user.email);
    I.fillField(this.fields.password, user.password);
  },

  submitLogin(){
    I.click(this.loginButton) 
  },
}